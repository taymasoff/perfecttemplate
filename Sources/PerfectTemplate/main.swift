//
//  main.swift
//  PerfectTemplate
//
//  Created by Kyle Jessup on 2015-11-05.
//	Copyright (C) 2015 PerfectlySoft, Inc.
//
//===----------------------------------------------------------------------===//
//
// This source file is part of the Perfect.org open source project
//
// Copyright (c) 2015 - 2016 PerfectlySoft Inc. and the Perfect project authors
// Licensed under Apache License v2.0
//
// See http://perfect.org/licensing.html for license information
//
//===----------------------------------------------------------------------===//
//

import PerfectHTTP
import PerfectHTTPServer
import Foundation

// An example request handler.
// This 'handler' function can be referenced directly in the configuration below.
func handler(request: HTTPRequest, response: HTTPResponse) {
	// Respond with a simple message.
	response.setHeader(.contentType, value: "text/html")
	response.appendBody(string: "<html><title>Hello, world!</title><body>Hello, world!</body></html>")
	// Ensure that response.completed() is called when your processing is done.
	response.completed()
}

// Configure one server which:
//	* Serves the hello world message at <host>:<port>/
//	* Serves static files out of the "./webroot"
//		directory (which must be located in the current working directory).
//	* Performs content compression on outgoing data when appropriate.
var routes = Routes()
routes.add(method: .get, uri: "/", handler: handler)

// TODO: - Вынести замыкания в отдельные методы и разнести по файлам

// MARK: - Login

routes.add(method: .post, uri: "/login") { request, response in
    
    guard request.param(name: "username") != nil
        && request.param(name: "password") != nil
    else {
        response.completed(status: HTTPResponseStatus.custom(code: 401, message: "Wrong user data"))
        return
    }
    
    let responseData: [String : Any] = [
        "result": 1,
        "user": [
            "id_user": 1,
            "user_login": "admin",
            "user_name": "John",
            "user_lastname": "Smith"
        ]
    ]
    
    do {
        try response.setBody(json: responseData, skipContentType: true)
        response.completed()
    } catch {
        response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
    }
}

// MARK: - Logout

routes.add(method: .get, uri: "/logout") { request, response in
    
    guard request.param(name: "id_user") != nil else {
            response.completed(status: HTTPResponseStatus.custom(code: 400, message: "Wrong user ID"))
            return
    }
    
    let responseData: [String : Any] = [
        "result": 1
    ]
    
    do {
        try response.setBody(json: responseData, skipContentType: true)
        response.completed()
    } catch {
        response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
    }
}

// MARK: - Register

routes.add(method: .post, uri: "/register") { request, response in
    
    guard request.param(name: "userData") != nil else {
            response.completed(status: HTTPResponseStatus.custom(code: 400, message: "Wrong user data"))
            return
    }
    
    let responseData: [String : Any] = [
        "result": 1,
        "userMessage": "Registration success"
    ]
    
    do {
        try response.setBody(json: responseData, skipContentType: true)
        response.completed()
    } catch {
        response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
    }
}

// MARK: - ChangeUserData

routes.add(method: .post, uri: "/changeuserdata") { request, response in
    
    guard request.param(name: "userData") != nil else {
            response.completed(status: HTTPResponseStatus.custom(code: 400, message: "Wrong user data"))
            return
    }
    
    let responseData: [String : Any] = [
        "result": 1
    ]
    
    do {
        try response.setBody(json: responseData, skipContentType: true)
        response.completed()
    } catch {
        response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
    }
}

// MARK: - Add To Basket

routes.add(method: .get, uri: "/addtobasket") { request, response in
    
    guard request.param(name: "id_product") != nil
        && request.param(name: "quantity") != nil
        else {
            response.completed(status: HTTPResponseStatus.custom(code: 400, message: "Wrong data"))
            return
    }
    
    let responseData: [String : Any] = [
        "result": 1
    ]
    
    do {
        try response.setBody(json: responseData, skipContentType: true)
        response.completed()
    } catch {
        response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
    }
}

// MARK: - Delete From Basket

routes.add(method: .get, uri: "/deletefrombasket") { request, response in
    
    guard request.param(name: "id_product") != nil else {
            response.completed(status: HTTPResponseStatus.custom(code: 400, message: "Wrong data"))
            return
    }
    
    let responseData: [String : Any] = [
        "result": 1
    ]
    
    do {
        try response.setBody(json: responseData, skipContentType: true)
        response.completed()
    } catch {
        response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
    }
}

// MARK: - Get Catalog

routes.add(method: .get, uri: "/getcatalog") { request, response in
    
    guard request.param(name: "page_number") != nil
        && request.param(name: "id_category") != nil
        else {
            response.completed(status: HTTPResponseStatus.custom(code: 400, message: "Wrong data"))
            return
    }
    
    let responseData: [[String : Any]] = [
        [
            "id_product": 4,
            "product_name": "Shovel",
            "price": 1255
        ],
        [
            "id_product": 7,
            "product_name": "Ball",
            "price": 100
        ]
    ]
    
    do {
        try response.setBody(json: responseData, skipContentType: true)
        response.completed()
    } catch {
        response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
    }
}

// MARK: - Get Basket

routes.add(method: .get, uri: "/getbasket") { request, response in
    
    guard request.param(name: "id_user") != nil else {
            response.completed(status: HTTPResponseStatus.custom(code: 401, message: "Wrong user data"))
            return
    }
    
    let responseData: [String : Any] = [
        "amount": 5,
        "countGoods": 2,
        "contents": [
            "product": [
                "id_product": 4,
                "product_name": "Shovel",
                "price": 1255
            ],
            "quantity": 1
        ]
    ]
    
    do {
        try response.setBody(json: responseData, skipContentType: true)
        response.completed()
    } catch {
        response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
    }
}

routes.add(method: .get, uri: "/**",
		   handler: StaticFileHandler(documentRoot: "./webroot", allowResponseFilters: true).handleRequest)
try HTTPServer.launch(name: "localhost",
					  port: 8181,
					  routes: routes,
					  responseFilters: [
						(PerfectHTTPServer.HTTPFilter.contentCompression(data: [:]), HTTPFilterPriority.high)])

